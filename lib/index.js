import Window from './Window';
import Output from './Output';

const run = (
    path = (require('electron').remote.app.isPackaged ? __dirname + '/../default.js' : './default.js')
) => eval(`
    (async () => {
        const { Window, Output } = this;
        const delay = delay => new Promise(resolve => setTimeout(resolve, delay));
        ${require('fs').readFileSync(path, 'UTF-8')}
    })();
`);

Object.freeze(Window);
Object.freeze(Output);
Object.freeze(run);

export {
    Window,
    Output,
    run
};