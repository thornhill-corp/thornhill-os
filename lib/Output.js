import store from '../src/store';

export default {
    /**
     * @method clear
     * @description Clear output
     */
    clear: () => store.commit('clearOutput'),

    /**
     * @method replace
     * @description Replace (non)-existing text output by new one
     * @param {String} text
     * @param {Boolean} [isOverlay=false]
     */
    replace: (text, isOverlay = false) => {
        if(typeof text !== 'string') throw new TypeError('text : Expected string');
        if(text.length === 0) throw new Error('text : Expected non-empty string');

        if(typeof isOverlay !== 'boolean') throw new Error('isOverlay : Expected boolean');
        if(isOverlay === true && text.includes('\n')) throw new Error('text : Expected single line string on overlay mode');

        store.commit('replaceOutput', [text, isOverlay]);
    },

    /**
     * @method append
     * @description Append output text to existing one
     * @param {String} text
     */
    append: text => {
        if(typeof text !== 'string') throw new TypeError('Expected string');
        if(text.length === 0) throw new Error('Expected non-empty string');
        if(store.state.output.length === 0) throw new Error('Expected existing output');
        if(store.state.isOutputOverlay === true) throw new Error('Expected non-overlay mode');
        store.commit('appendOutput', text);
    }
}