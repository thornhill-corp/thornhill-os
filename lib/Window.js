import store from '../src/store';
import colors from '../src/helpers/colors';

/**
 * @private
 * @class WindowBodyElement
 * @description Window body element
 * @param {String} type - Element type
 * @param {Object} props - Element properties
 */
const WindowBodyElement = function(type, props){
    this.constructor = WindowBodyElement;
    Object.defineProperty(this, 'props', {
        value: Object.defineProperties({}, (() => {
            const o = {
                is: {
                    value: type,
                    enumerable: true
                }
            };
            if(typeof props === 'object')
                Object.keys(props).forEach(key =>
                    o[key] = {
                        get: () => props[key],
                        enumerable: true
                    });
            return o;
        })())
    });
};

/**
 * @class Window
 * @description Machine PoV interface 'window' element
 * @param {Number} positionX - Window position in pixels relative to the left side of the screen
 * @param {Number} positionY - Window position in pixels relative to the top side of the screen
 * @param {String} titleText - Window title text
 * @param {String} [titleColor=white] - Window title color
 * @param {Boolean} [isTitleLoading=false] - Window title loading state
 * @param {Boolean} [isTitleWarning=false] - Window title warning state
 * @param {String} [subtitleText] - Window subtitle text
 * @param {String} [subtitleMetaText] - Window subtitle meta text
 * @param {Boolean} [isSubtitleLoading=false] - Window subtitle loading state
 * @param {Boolean} [isSubtitleError=false] - Window subtitle error state
 * @param {Number} [progress] - Window subtitle progress parcentage
 * @param {Number} [minWidth] - Window minimal width in pixels
 */
const Window = function(
    positionX,
    positionY,
    titleText,
    titleColor = 'white',
    isTitleLoading = false,
    isTitleWarning = false,
    subtitleText,
    subtitleMetaText,
    isSubtitleLoading = false,
    isSubtitleError = false,
    progress,
    minWidth = 0
){
    if(typeof positionX !== 'number') throw new TypeError('positionX : Expected number');
    if(positionX < 0) throw new TypeError('positionX : Expected positive number');

    if(typeof positionY !== 'number') throw new TypeError('positionY : Expected number');
    if(positionY < 0) throw new TypeError('positionY : Expected positive number');

    if(typeof titleText !== 'string') throw new TypeError('titleText : Expected string');
    if(titleText.length === 0) throw new TypeError('titleText : Expected non-empty string');

    if(typeof titleColor !== 'string') throw new TypeError('titleColor : Expected string');
    if(!colors.validate(titleColor)) throw new TypeError('titleColor : Expected valid color');

    if(typeof isTitleLoading !== 'boolean') throw new TypeError('isTitleLoading : Expected boolean');

    if(typeof isTitleWarning !== 'boolean') throw new TypeError('isTitleWarning : Expected boolean');

    if(typeof subtitleText !== 'undefined'){
        if(typeof subtitleText !== 'string') throw new TypeError('subtitleText : Expected string');
        if(subtitleText.length === 0) throw new TypeError('subtitleText : Expected non-empty string');
    }

    if(typeof subtitleMetaText !== 'undefined'){
        if(typeof subtitleMetaText !== 'string') throw new TypeError('subtitleMetaText : Expected string');
        if(subtitleMetaText.length === 0) throw new TypeError('subtitleMetaText : Expected non-empty string');
    }

    if(typeof isSubtitleLoading !== 'boolean') throw new TypeError('isSubtitleLoading : Expected boolean');

    if(typeof isSubtitleError !== 'boolean') throw new TypeError('isSubtitleError : Expected boolean');

    if(typeof progress !== 'undefined'){
        if(typeof subtitleText === 'string') throw new Error('progress : Expected no subtitle');
        if(typeof progress !== 'number') throw new TypeError('progress : Expected number');
        if(progress < 0) throw new RangeError('progress : Expected positive number');
        if(progress > 100) throw new RangeError('progress : Expected number lower or equal 100');
    }

    if(typeof minWidth !== 'number') throw new TypeError('minWidth : Expected number');

    Window.windows.push(this);

    const body = [];

    const _size = {
        width: 0,
        height: 0
    };

    const _el = {
        isShown: false,
        positionX,
        positionY,
        titleText,
        titleColor,
        isTitleLoading,
        isTitleWarning,
        subtitleText,
        isSubtitleLoading,
        isSubtitleError,
        subtitleMetaText,
        progress,
        body: [],
        minWidth,
        setSize: ({ width, height }) => {
            _size.width = width;
            _size.height = height;
        }
    };

    store.commit('addWindow', _el);

    const refreshBody = () => _el.body = body.map(item => item.props);

    this.isShown = () => _el.isShown;

    this.isDestroyed = () => !store.state.windows.includes(_el) || !Window.windows.includes(this);

    this.getSize = () => ({
        width: _size.width,
        height: _size.height
    });

    /**
     * @method show
     * @description Add window to the UI view
     */
    this.show = () => {
        if(this.isShown()) throw new Error('Expected window hidden from view');
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        _el.isShown = true;
    };

    /**
     * @method hide
     * @description Remove window from the UI view
     */
    this.hide = () => {
        if(!this.isShown()) throw new Error('Expected window shown in view');
        _el.isShown = false;
    };

    /**
     * @method destroy
     * @escroption Remove window from memory
     */
    this.destroy = () => {
        if(this.isShown()) throw new Error('Expected window hidden from view');
        if(this.isDestroyed()) throw new Error('Expected window not destroyed');
        store.commit('removeWindow', _el);
        Window.windows.splice(Window.windows.indexOf(this), 1);
    };

    this.getPositionX = () => _el.positionX;

    /**
     * @method setPositionX
     * @description Set window position in pixels relative to the left side of the screen
     * @param {Number} positionX
     */
    this.setPositionX = positionX => {
        if(typeof positionX !== 'number') throw new TypeError('Expected number');
        if(positionX < 0) throw new RangeError('Expected positive number');
        _el.positionX = positionX;
    };

    this.getPositionY = () => _el.positionY;

    /**
     * @method setPositionY
     * @description Set window position in pixels relative to the top side of the screen
     * @param {Number} positionY
     */
    this.setPositionY = positionY => {
        if(typeof positionY !== 'number') throw new TypeError('Expected number');
        if(positionY < 0) throw new RangeError('Expected positive number');
        _el.positionY = positionY;
    };

    /**
     * @method setRandomPosition
     * @description Randomly set window position
     */
    this.setRandomPosition = async () => {
        if(this.isShown()) throw new Error('Expected window hidden from view');
        while(!_size.width || !_size.height)
            await new Promise(resolve => setTimeout(resolve, 0));
        const
            random = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min,
            {
                width: spaceWidth,
                height: spaceHeight
            } = store.state.spaceSize,
            {
                width: windowWidth,
                height: windowHeight
            } = _size;
        let i = 0;
        do {
            if(++i > 1000) throw new Error('Expected empty space available');
            this.setPositionX(random(0, spaceWidth - windowWidth));
            this.setPositionY(random(0, spaceHeight - windowHeight));
        } while(Window.windows.find(window => window !== this && window.isColliding(this)));
    };

    this.getTitleText = () => _el.titleText;

    /**
     * @method setTitleText
     * @description Set window title
     * @param {String} titleText
     */
    this.setTitleText = titleText => {
        if(typeof titleText !== 'string') throw new TypeError('Expected string');
        if(titleText.length === 0) throw new TypeError('Expected non-empty string');
        _el.titleText = titleText;
    };

    this.getTitleColor = () => _el.titleColor;

    /**
     * @method setTitleColor
     * @description Set window title color
     * @param {String} titleColor
     */
    this.setTitleColor = titleColor => {
        if(typeof titleColor !== 'string') throw new TypeError('Expected string');
        if(!colors.validate(titleColor)) throw new TypeError('Expected valid color');
        _el.titleColor = titleColor;
    };

    this.isTitleLoading = () => _el.isTitleLoading;

    /**
     * @method setTitleLoading
     * @description Set window title loading state
     * @param {Boolean} isTitleLoading
     */
    this.setTitleLoading = isTitleLoading => {
        if(typeof isTitleLoading !== 'boolean') throw new TypeError('Expected boolean');
        _el.isTitleLoading = isTitleLoading;
    };

    this.isTitleWarning = () => _el.isTitleWarning;

    /**
     * @method setTitleWarning
     * @description Set window title warning state
     * @param {Boolean} isTitleWarning
     */
    this.setTitleWarning = isTitleWarning => {
        if(typeof isTitleWarning !== 'boolean') throw new TypeError('Expected boolean');
        _el.isTitleWarning = isTitleWarning;
    };

    this.getSubtitleText = () => _el.subtitleText;

    /**
     * @method setSubtitleText
     * @description Set window subtitle text
     * @param {String} subtitleText
     */
    this.setSubtitleText = subtitleText => {
        if(typeof subtitleText !== 'string') throw new TypeError('Expected string');
        if(subtitleText.length === 0) throw new TypeError('Expected non-empty string');
        _el.subtitleText = subtitleText;
    };

    this.isSubtitleLoading = () => _el.isSubtitleLoading;

    /**
     * @method setSubtitleLoading
     * @description Set window subtitle loading state
     * @param {Boolean} isSubtitleLoading
     */
    this.setSubtitleLoading = isSubtitleLoading => {
        if(typeof isSubtitleLoading !== 'boolean') throw new TypeError('Expected boolean');
        _el.isSubtitleLoading = isSubtitleLoading;
    };

    this.isSubtitleError = () => _el.isSubtitleError;

    /**
     * @method setSubtitleLoading
     * @description Set window subtitle error state
     * @param {Boolean} isSubtitleError
     */
    this.setSubtitleError = isSubtitleError => {
        if(typeof isSubtitleError !== 'boolean') throw new TypeError('Expected boolean');
        _el.isSubtitleError = isSubtitleError;
    };

    this.getSubtitleMetaText = () => _el.subtitleMetaText;

    /**
     * @method setSubtitleMetaText
     * @description Set window subtitle meta text
     * @param {String} subtitleMetaText
     *
     */
    this.setSubtitleMetaText = subtitleMetaText => {
        if(typeof subtitleMetaText !== 'string') throw new TypeError('Expected string');
        if(subtitleMetaText.length === 0) throw new TypeError('Expected non-empty string');
        _el.subtitleMetaText = subtitleMetaText;
    };

    /**
     * @method unsetSubtitleMetaText
     * @description Remove window subtitle meta text
     */
    this.unsetSubtitleMetaText = () => _el.subtitleMetaText = undefined;

    /**
     * @method unsetSubtitle
     * @description Remove window subtitle
     * (resets subtitle text + subtitle loading state + subtitle meta text)
     */
    this.unsetSubtitle = () => {
        _el.subtitleText = undefined;
        _el.isSubtitleLoading = undefined;
        _el.subtitleMetaText = undefined;
    };

    this.getProgress = () => _el.progress;

    /**
     * @method setProgress
     * @description Set window progress bar
     * (this is only available when window subtitle is not set)
     * @param {Number} progress - Percentage
     */
    this.setProgress = progress => {
        if(typeof _el.subtitleText === 'string') throw new Error('Expected no subtitle');
        if(typeof progress !== 'number') throw new TypeError('Expected number');
        if(progress < 0) throw new RangeError('Expected positive number');
        if(progress > 100) throw new RangeError('Expected number lower or equal 100');
        _el.progress = progress;
    };

    /**
     * @method unsetProgress
     * @description Remove window progress bar
     */
    this.unsetProgress = () => _el.progress = undefined;

    /**
     * @method addElement
     * @description Add element to body
     * @param {WindowBodyElement} element
     * @param {Number} [position] - Default : end
     */
    this.addElement = (element, position = body.length) => {
        if(element.constructor !== WindowBodyElement) throw new TypeError('Element : Expected window body element');
        if(body.includes(element)) throw new Error('Expected element not in window body');
        if(typeof position !== 'number' || !Number.isInteger(position)) throw new TypeError('position : Expected integer');
        if(position < 0) throw new RangeError('position : Expected positive number');
        if(position > body.length) throw new RangeError('position : Expected below element count');
        body.splice(position, 0, element);
        refreshBody();
    };

    /**
     * @method removeElement
     * @description Remove element from body
     * @param {WindowBodyElement} element
     */
    this.removeElement = element => {
        if(!body.includes(element)) throw new Error('Expected element in window body');
        body.splice(body.indexOf(element), 1);
        refreshBody();
    };

    /**
     * @method getElement
     * @description Get element at position
     * @param {Number} position
     * @returns WindowBodyElement
     */
    this.getElement = position => {
        if(typeof position !== 'number') throw new Error('position : Expected number');
        if(position < 0) throw new RangeError('position : Expected positive number');
        if(position >= body.length) throw new RangeError('position : Expected below last element position');
        if(!Number.isInteger(position)) throw new Error('position : Expected integer');
        return body[position];
    };

    this.getMinWidth = () => _el.minWidth;

    /**
     * @method minWidth
     * @description Set window minimal width
     * @param {Number} minWidth - Pixels
     */
    this.setMinWidth = minWidth => {
        if(typeof minWidth !== 'number') throw new TypeError('Expected number');
        _el.minWidth = minWidth;
    };

    /**
     * @method isColliding
     * @description Check if window collides with another window
     * @param {Window} window
     */
    this.isColliding = window => {
        const
            x1 = this.getPositionX(),
            y1 = this.getPositionY(),
            {
                width: w1,
                height: h1
            } = this.getSize(),
            x2 = window.getPositionX(),
            y2 = window.getPositionY(),
            {
                width: w2,
                height: h2
            } = window.getSize();
        return (
            x1 + w1 >= x2
            &&
            x1 <= x2 + w2
            &&
            y1 + h1 >= y2
            &&
            y1 <= y2 + h2
        );
    };
};

Window.windows = [];

/**
 * @class Separator
 */
Window.Separator = function(){
    WindowBodyElement.call(this, 'WindowBodySeparator');
};

/**
 * @class Subtitle
 * @param {String} text - Subtitle text
 * @param {Boolean} [isLoading=false] - Subtitle loading state
 */
Window.Subtitle = function(text, isLoading = false){
    if(typeof text !== 'string') throw new TypeError('text : Expected string');
    if(text.length === 0) throw new TypeError('text : Expected non-empty string');

    if(typeof isLoading !== 'boolean') throw new TypeError('isLoading : Expected boolean');

    const _el = {
        text,
        isLoading
    };

    WindowBodyElement.call(this, 'WindowBodySubtitle', _el);

    this.getText = () =>_el.text;

    /**
     * @method setText
     * @description Set subtitle text
     * @param {String} text
     */
    this.setText = text => {
        if(typeof text !== 'string') throw new TypeError('Expected string');
        if(text.length === 0) throw new TypeError('Expected non-empty string');
        _el.text = text;
    };

    this.isLoading = () => _el.isLoading;

    /**
     * @method setLoading
     * @description Set subtitle loading state
     * @param {Boolean} isLoading
     */
    this.setLoading = isLoading => {
        if(typeof isLoading !== 'boolean') throw new TypeError('Expected boolean');
        _el.isLoading = isLoading;
    };
};

/**
 * @class Text
 * @param {String} text - Text content
 * @param {Boolean} [isLoading=false] - Text loading state
 * @param {String} [color=white] - Text color
 * @param {String} [backgroundColor=transparent] - Text background color
 */
Window.Text = function(text, isLoading = false, color = 'white', backgroundColor = 'transparent'){
    if(typeof text !== 'string') throw new TypeError('text : Expected string');
    if(text.length === 0) throw new TypeError('text : Expected non-empty string');

    if(typeof isLoading !== 'boolean') throw new TypeError('isLoading : Expected boolean');

    if(typeof color !== 'string') throw new TypeError('color : Expected string');
    if(!colors.validate(color)) throw new TypeError('color : Expected valid color');

    if(typeof backgroundColor !== 'string') throw new TypeError('backgroundColor : Expected string');
    if(!colors.validate(backgroundColor)) throw new TypeError('backgroundColor : Expected valid color');

    const _el = {
        text,
        isLoading,
        color,
        backgroundColor
    };

    WindowBodyElement.call(this, 'WindowBodyText', _el);

    this.getText = () =>_el.text;

    /**
     * @method setText
     * @description Set text content
     * @param {String} text
     */
    this.setText = text => {
        if(typeof text !== 'string') throw new TypeError('Expected string');
        if(text.length === 0) throw new TypeError('Expected non-empty string');
        _el.text = text;
    };

    this.isLoading = () => _el.isLoading;

    /**
     * @method setLoading
     * @description Set text loading state
     * @param {Boolean} isLoading
     */
    this.setLoading = isLoading => {
        if(typeof isLoading !== 'boolean') throw new TypeError('Expected boolean');
        _el.isLoading = isLoading;
    };

    this.getColor = () => _el.color;

    /**
     * @method setColor
     * @description Set text color
     * @param {String} color
     */
    this.setColor = color => {
        if(typeof color !== 'string') throw new TypeError('Expected string');
        if(!colors.validate(color)) throw new TypeError('Expected valid color');
        _el.color = color;
    };

    this.getBackgroundColor = () => _el.backgroundColor;

    /**
     * @method setBackgroundColor
     * @description Set text background color
     * @param {String} backgroundColor
     */
    this.setBackgroundColor = backgroundColor => {
        if(typeof backgroundColor !== 'string') throw new TypeError('Expected string');
        if(!colors.validate(backgroundColor)) throw new TypeError('Expected valid color');
        _el.backgroundColor = backgroundColor;
    };
};

/**
 * @class Picture
 * @param {String} url - Picture link
 * @param {Number} [width] - Picture width in pixels
 * @param {Number} [height] - Picture height in pixels
 */
Window.Picture = function(url, width, height){

    if(typeof url !== 'string') throw new TypeError('url : Expected string');
    if(url.length === 0) throw new TypeError('url : Expected non-empty string');

    if(typeof width !== 'undefined'){
        if(typeof width !== 'number') throw new TypeError('width : Expected number');
        if(width < 0) throw new RangeError('width : Expected positive number');
    }

    if(typeof height !== 'undefined'){
        if(typeof height !== 'number') throw new TypeError('height : Expected number');
        if(height < 0) throw new RangeError('height : Expected positive number');
    }

    const _el = {
        url,
        width,
        height
    };

    WindowBodyElement.call(this, 'WindowBodyPicture', _el);

    this.getUrl = () => _el.url;

    /**
     * @method setUrl
     * @description Set picture link
     * @param {String} url
     */
    this.setUrl = url => {
        if(typeof url !== 'string') throw new TypeError('Expected string');
        if(url.length === 0) throw new TypeError('Expected non-empty string');
        _el.url = url;
    };

    this.getWidth = () => _el.width;

    /**
     * @method setWidth
     * @description Set picture width
     * @param {Number} width - Pixels
     */
    this.setWidth = width => {
        if(typeof width !== 'number') throw new TypeError('Expected number');
        if(width < 0) throw new RangeError('Expected positive number');
        _el.width = width;
    };

    this.getHeight = () => _el.height;

    /**
     * @method setHeight
     * @description Set picture height
     * @param {Number} height - Pixels
     */
    this.setHeight = height => {
        if(typeof height !== 'number') throw new TypeError('Expected number');
        if(height < 0) throw new RangeError('Expected positive number');
        _el.height = height;
    };
};

Window.Picture.NO_PHOTO_AVAILABLE = 'NO_PHOTO_AVAILABLE';

/**
 * @class Meter
 * @param {String} label - Meter label
 * @param {Number} value - Meter value in percent
 * @param {String} backgroundColor - Meter background color
 */
Window.Meter = function(label, value, backgroundColor = 'white'){
    if(typeof label !== 'undefined'){
        if(typeof label !== 'string') throw new TypeError('label : Expected string');
        if(label.length === 0) throw new TypeError('label : Expected non-empty string');
    }

    if(typeof value !== 'number') throw new TypeError('value : Expected number');
    if(value < 0) throw new RangeError('value : Expected positive number');
    if(value > 100) throw new RangeError('value : Expected number lower or equal 100');

    if(typeof backgroundColor !== 'string') throw new TypeError('backgroundColor : Expected string');
    if(!colors.validate(backgroundColor)) throw new TypeError('backgroundColor : Expected valid color');

    const _el = {
        label,
        value,
        backgroundColor
    };

    WindowBodyElement.call(this, 'WindowBodyMeter', _el);

    this.getLabel = () => _el.label;

    /**
     * @method setLabel
     * @description Set meter label
     * @param {String} label
     */
    this.setLabel = label => {
        if(typeof label !== 'string') throw new TypeError('Expected string');
        if(label.length === 0) throw new TypeError('Expected non-empty string');
        _el.label = label;
    };

    /**
     * @method unsetLabel
     * @description Remove meter label
     */
    this.unsetLabel = () => _el.label = undefined;

    this.getValue = () => _el.value;

    /**
     * @method setValue
     * @description Set meter value in percent
     * @param {Number} value - Percent
     */
    this.setValue = value => {
        if(typeof value !== 'number') throw new TypeError('Expected number');
        if(value < 0) throw new RangeError('Expected positive number');
        if(value > 100) throw new RangeError('Expected number lower or equal 100');
        _el.value = value;
    };

    this.getBackgroundColor = () => _el.backgroundColor;

    /**
     * @method setBackgroundColor
     * @description Set meter background color
     * @param {String} backgroundColor
     */
    this.setBackgroundColor = backgroundColor => {
        if(typeof backgroundColor !== 'string') throw new TypeError('backgroundColor : Expected string');
        if(!colors.validate(backgroundColor)) throw new TypeError('Expected valid color');
        _el.backgroundColor = backgroundColor;
    };
};

/**
 * @typedef WindowTableColumn
 * @type Object
 * @property {String} text
 * @property {String} color
 */

/**
 * @typedef WindowTableLine
 * @type {WindowTableColumn[]}
 */

/**
 * @typedef WindowTableData
 * @type {WindowTableLine[]}
 */

/**
 * @class Table
 * @param {String} [titleText] - Table title
 * @param {WindowTableData} data - Table body data
 */
Window.Table = function(titleText, data){
    if(typeof titleText !== 'undefined'){
        if(typeof titleText !== 'string') throw new TypeError('titleText : Expected string');
        if(titleText.length === 0) throw new TypeError('titleText : Expected non-empty string');
    }

    if(!Array.isArray(data)) throw new TypeError('data : Expected array');
    for(let i = 0; i < data.length; i++){
        if(!Array.isArray(data[i])) throw new TypeError(`data[${i}] : Expected array`);
        if(data[i].length === 0) throw new TypeError(`data[${i}] : Expected non-empty array`);
        for(let j = 0; j < data[i].length; j++){
            if(typeof data[i][j] !== 'object') throw new TypeError(`data[${i}][${j}] : Expected object`);
            if(typeof data[i][j].text !== 'string') throw new TypeError(`data[${i}][${j}].text : Expected string`);
            if(data[i][j].text.length === 0) throw new TypeError(`data[${i}][${j}].text : Expected non-empty string`);
            if(typeof data[i][j].color !== 'undefined'){
                if(typeof data[i][j].color !== 'string') throw new TypeError(`data[${i}][${j}].color : Expected string`);
                if(!colors.validate(data[i][j].color)) throw new TypeError(`data[${i}][${j}].color : Expected valid color`);
            }
        }
    }

    const _el = {
        titleText,
        data
    };

    WindowBodyElement.call(this, 'WindowBodyTable', _el);

    this.getTitleText = () => _el.titleText;

    /**
     * @method setTitleText
     * @description Set table title text
     * @param {String} titleText
     */
    this.setTitleText = titleText => {
        if(typeof titleText !== 'string') throw new TypeError('Expected string');
        if(titleText.length === 0) throw new TypeError('Expected non-empty string');
        _el.titleText = titleText;
    };

    /**
     * @method unsetTitleText
     * @description Remove table title text
     */
    this.unsetTitleText = () => _el.titleText = undefined;

    this.getData = () => JSON.parse(JSON.stringify(_el.data));

    /**
     * @method setData
     * @description Set table body data
     * @param {WindowTableData} data
     */
    this.setData = data => {
        if(!Array.isArray(data)) throw new TypeError('data : Expected array');
        for(let i = 0; i < data.length; i++){
            if(!Array.isArray(data[i])) throw new TypeError(`data[${i}] : Expected array`);
            if(data[i].length === 0) throw new TypeError(`data[${i}] : Expected non-empty array`);
            for(let j = 0; j < data[i].length; j++){
                if(typeof data[i][j] !== 'object') throw new TypeError(`data[${i}][${j}] : Expected object`);
                if(typeof data[i][j].text !== 'string') throw new TypeError(`data[${i}][${j}].text : Expected string`);
                if(data[i][j].text.length === 0) throw new TypeError(`data[${i}][${j}].text : Expected non-empty string`);
                if(typeof data[i][j].color !== 'undefined'){
                    if(typeof data[i][j].color !== 'string') throw new TypeError(`data[${i}][${j}].color : Expected string`);
                    if(!colors.validate(data[i][j].color)) throw new TypeError(`data[${i}][${j}].color : Expected valid color`);
                }
            }
        }
        _el.data.splice(0, _el.data.length, ...data);
    };

    /**
     * @method getLine
     * @description Get table line at position
     * @param {Number} position
     */
    this.getLine = position => {
        if(typeof position !== 'number') throw new Error('position : Expected number');
        if(position < 0) throw new RangeError('position : Expected positive number');
        if(position >= _el.data.length) throw new RangeError('position : Expected below last element position');
        if(!Number.isInteger(position)) throw new Error('position : Expected integer');
        return JSON.parse(JSON.stringify(_el.data[position]));
    };

    /**
     * @method addLine
     * @description Add line to table body
     * @param {WindowTableLine} line
     * @param {Number} [position] - Default : end
     */
    this.addLine = (line, position = _el.data.length) => {
        if(!Array.isArray(line)) throw new TypeError('line : Expected array');
        if(line.length === 0) throw new TypeError('line : Expected non-empty array');
        for(let i = 0; i < line.length; i++){
            if(typeof line[i] !== 'object') throw new TypeError(`line[${i}] : Expected object`);
            if(typeof line[i].text !== 'string') throw new TypeError(`line[${i}].text : Expected string`);
            if(line[i].text.length === 0) throw new TypeError(`line[${i}].text : Expected non-empty string`);
            if(typeof line[i].color !== 'undefined'){
                if(typeof line[i].color !== 'string') throw new TypeError(`line[${i}].color : Expected string`);
                if(!colors.validate(line[i].color)) throw new TypeError(`line[${i}].color : Expected valid color`);
            }
        }
        _el.data.splice(position, 0, line);
         // _el.data.push(line);
    };

    /**
     * @method removeLine
     * @description Remove line from table body
     * @param {Number} position
     */
    this.removeLine = position => {
        if(!Array.isArray(_el.data[position])) throw new RangeError('Expected existing line position');
        _el.data.splice(position, 1);
    };

    /**
     * @method replaceLine
     * @description Replace existing line in table body
     * @param {Number} position
     * @param {WindowTableLine} line
     */
    this.replaceLine = (position, line) => {
        if(!Array.isArray(_el.data[position])) throw new RangeError('Expected existing line position');
        for(let i = 0; i < line.length; i++){
            if(typeof line[i] !== 'object') throw new TypeError(`line[${i}] : Expected object`);
            if(typeof line[i].text !== 'string') throw new TypeError(`line[${i}].text : Expected string`);
            if(line[i].text.length === 0) throw new TypeError(`line[${i}].text : Expected non-empty string`);
            if(typeof line[i].color !== 'undefined'){
                if(typeof line[i].color !== 'string') throw new TypeError(`line[${i}].color : Expected string`);
                if(!colors.validate(line[i].color)) throw new TypeError(`line[${i}].color : Expected valid color`);
            }
        }
        _el.data.splice(position, 1, line);
    };
};

/**
 * @class Countdown
 * @param {Number} time - Countdown end time
 * @param {Boolean} [isStoppingAtZero=true] - Stop countdown at 00:00:00.0 or display negative value
 */
Window.Countdown = function(time, isStoppingAtZero = true){
    if(typeof time !== 'number') throw new TypeError('time : Expected number');

    if(typeof isStoppingAtZero !== 'boolean') throw new TypeError('isStoppingAtZero : Expected boolean');

    const _el = {
        time,
        isStoppingAtZero
    };

    WindowBodyElement.call(this, 'WindowBodyCountdown', _el);

    this.isTimedOut = () => Date.now() > _el.time;

    this.getTime = () => _el.time;

    /**
     * @method setTime
     * @description Set countdown end time
     * @param time
     */
    this.setTime = time => {
        if(this.isTimedOut()) throw new Error('Expected not timed out');
        if(typeof time !== 'number') throw new TypeError('Expected number');
        _el.time = time;
    };

    this.isStoppingAtZero = () => _el.isStoppingAtZero;

    /**
     * @method setStoppingAtZero
     * @description Set wether to stop countdown at 00:00:00.0 or display negative value
     * @param isStoppingAtZero
     */
    this.setStoppingAtZero = isStoppingAtZero => {
        if(this.isTimedOut()) throw new Error('Expected not timed out');
        if(typeof isStoppingAtZero !== 'boolean') throw new TypeError('Expected boolean');
        _el.isStoppingAtZero = isStoppingAtZero;
    }
};

/**
 * @class ScrollingData
 * @param {String} data
 * @param {Number} [width=200] - Container width in pixels
 * @param {Number} [height=150] - Container height in pixels
 */
Window.ScrollingData = function(data, width = 250, height = 150){
    if(typeof data !== 'string') throw new TypeError('data : Expected string');

    if(typeof width !== 'number') throw new TypeError('width : Expected number');
    if(width < 0) throw new RangeError('width : Expected positive number');

    if(typeof height !== 'number') throw new TypeError('height : Expected number');
    if(height < 0) throw new RangeError('height : Expected positive number');

    const _el = {
        data,
        width,
        height
    };

    WindowBodyElement.call(this, 'WindowBodyScrollingData', _el);

    this.getData = () => _el.data;

    /**
     * @method setData
     * @param {String} data
     */
    this.setData = data => {
        if(typeof data !== 'string') throw new TypeError('Expected string');
        _el.data = data;
    };

    this.getWidth = () => _el.width;

    /**
     * @method setWidth
     * @description Set container width in pixels
     * @param {Number} width
     */
    this.setWidth = width => {
        if(typeof width !== 'number') throw new TypeError('Expected number');
        if(width < 0) throw new RangeError('Expected positive number');
        _el.width = width;
    };

    this.getHeight = () => _el.height;

    /**
     * @method setHeight
     * @description Set container height in pixels
     * @param {Number} height
     */
    this.setHeight = height => {
        if(typeof height !== 'number') throw new TypeError('Expected number');
        if(height < 0) throw new RangeError('Expected positive number');
        _el.height = height;
    };
};

/**
 * @method dateToString
 * @param {Date} date
 * @returns {string}
 */
Window.dateToString = date => date.toLocaleDateString('en-US', {
    month: 'long',
    day: 'numeric',
    year: 'numeric'
});

export default Window;