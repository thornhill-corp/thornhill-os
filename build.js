const
    fs = require('fs'),
    Parcel = require('parcel-bundler'),
    electronPackager = require('electron-packager');

const options = {
    icon: './thornhill.ico',
    dir: __dirname,
    arch: 'x64',
    ignore: [
        '.gitignore',
        '.cache',
        '.idea',
        'build',
        'docs',
        'build.js',
        'README.md',
        'yarn.lock'
    ],
    out: 'build',
    asar: true,
    overwrite: true
};

(async () => {
    fs.rmdirSync('./dist', { recursive: true });
    await new Parcel('index.html', { target: 'electron', publicURL: '.' }).bundle();
    await Promise.all([
        electronPackager({
            ...options,
            platform: 'win32'
        }),
        electronPackager({
            ...options,
            platform: 'linux'
        })
    ]);
    process.exit();
})();