# Reference

Most of this software and its documentation refers to Person of Interest.

### Asset

Refers to the people serving the Machine's purpose.

### Open system

Refers to the access to MPOV and direct communication from assets computers.

### Admin

Refers to the Machine's creator.