# Modules

Modules are non-core libraries installed as a Node package and are executed on-demand when called by name from the terminal.

The name must be prefixed with `@thornhill-corp/` in the [package.json](/package.json) file.

## Creating a module

In order to run a module, the app will call the `default` method, and pass 3 parameters :
- `lib` : the library classes ([Window](Window.md), [Output](Output.md)) in an object
- `args` : the arguments, in an array
- `opts` : the options, in an object

The most intuitive implementation of this is :

```js
module.exports = {
    /**
     * @method createwindow
     * @description Display a window
     * @param Window
     * @param title - Window title
     * @param color - Window title color
     * @param help
     * @param h - Short for help
     * @param loading - Window title loading dots
     * @param l - Short for loading
     */
    default: ({ Window }, [ title, color ], { help, h, loading, l }) => {
        if(help || h){
            return `Create Window v${require('./package').version}`
                + `\nUsage : createwindow <title> [color] [-l / --loading] [-h / --help]`;
        }
        else {
            const w = new Window(0, 0, title);
            if(color) w.setTitleColor(color);
            if(loading || l) w.setTitleLoading(true);
            w.show();
        }
    }
}
```

From the terminal, this could be called that way : `createwindow "My Title" red --loading`

See [Terminal](Terminal.md) for more details regarding command parsing and usage description **(important)**.

When necessary, you can print some text response as `return` value.

Exporting additional methods can be useful to manipulate features and settings from other modules, scripts and devtools.

## Support

Anyone can create a module, but the list of officially supported modules is limited.

In order to be officially supported, a module must :
- be open source
- be hosted on git.kaki87.net
- not have runtime dependencies *(unless properly justified)*
- use base code provided above
    - JSDoc
    - destructured params
    - `if` statements
    - `--help` option
        - name
        - package.json version
        - usage
    - backticks

Then, you can create an issue prefixed `[Fork request]`. It will then be forked and added to the list.

### Officially supported modules list

- [discord](https://git.kaki87.net/thornhill-corp/discord) : Discord bot integration