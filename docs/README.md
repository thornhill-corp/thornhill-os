# Documentation

- [Reference](Reference.md)

## UI elements

- [Window](Window.md)
- [Output](Output.md)
- [Terminal](Terminal.md)

Some of these elements can be manipulated from the library API.

This API's classes can be accessed from a script or a module.

## Scripts

A script is a JavaScript file with some simplifying particulatiries :
- the arbitrary code is executed from within an `async` function so that `await` can be easily used
- a `delay()` function is pre-defined within this scope, so it can directly be used with `await`
- the library classes are pre-declared within this scope, so they can be directly used as is

Code reference and examples are available on each UI page in the *API* section.

### Default script

The default script, always located in the root directory and named `default.js`, always auto-runs with the app, unless it is empty.

*The app will throw an error if the file doesn't exist at all.*

### Run on-demand

Scripts can be run on-demand from the terminal.

See [terminal](Terminal.md).

## Modules

Modules are non-core libraries installed as a Node package.

See [modules](Modules.md).