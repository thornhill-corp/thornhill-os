# Output

The output designates the way the Machine directly communicates with its [assets](Reference.md#asset) by displaying text from the [open system](Reference.md#open-system).

## Full screen

![Full screen output](images/Output-1.jpg)

## Overlay

![Overlay output](images/Output-2.jpg)