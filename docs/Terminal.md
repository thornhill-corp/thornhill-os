# Terminal

The terminal is the way [admin](Reference.md#admin) controls the Machine in its development stage.

![](images/Terminal-1.jpg)
<br>*(software screenshot)*

## Behavior

Open the terminal by pressing *Space* and close it by pressing *Escape*.

The terminal behaves as much Bash-like as possible :
- access history using up/down arrow keys
- clear output using Ctrl + L
- cancel using Ctrl + C
- type comments by prefixing with `#`
- launch commands in background by suffixing with `&`

## Parsing

The terminal follows most of the [POSIX recommandations](https://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html) ([archive](https://web.archive.org/web/20200717182100/https://www.gnu.org/software/libc/manual/html_node/Argument-Syntax.html)) for arguments and options parsing.

For instance :
- `mycommand a1 a2 a3` will work
<br>*(`a1`, `a2` and `a3` being arguments)*
- `mycommand -t "some title"` will work
<br>*(`t` being an option with `some title` as value)*
- `mycommand --title "some title"` will work
<br>*(`title` being an option with `some title` as value)*
- `mycommand -abc` will work as `mycommand -a -b -c`, which also works
<br>*(`a`, `b` and `c` being options with value `true`)*

Moreover :
- `--long-option-name` will be transformed to `longOptionName` when passed to the handler (see [modules](Modules.md))

However :
- the `--title="some title"` syntax isn't supported
- the `--` terminative syntax isn't supported
- there is no validation control, then, a mistyped command may still be executed while invalid parameters might be wrongly understood or just ignored

## Help

Commands should have a `--help` (and maybe `-h`) option which outputs something like this :

```
My Command v1.0.0
Usage : mycommand <arg1> [arg2] [-o / --option] [--value] <value>
```

The first line indicates the module name and version, which is mandatory for reporting an issue.

The second line is the command's usage description :

- arguments between `<>` are required
- arguments between `[]` are optional
- options are always optional
- an argument following an option is used as the option's value
- options may only have a short name or only have a long name,
<br>both are specified in usage description when both work

Some modules may also provide multiple usages when several sub-commands are available.

## Commands

### Native commands

Exceptionally, the following commands will not provide a `--help` option, due to their absolute simplicity.

### `help`

List all available native and module commands.

### `about`

Displays the app version.

### `shutdown`

Exit the app.

### `run [path]`

Run a script.

Args:
- `path` The script's path
<br>*If not specified, the default script will re-execute*

### Module commands

Modules are executed on-demand when called by name.

See [modules](Modules.md).