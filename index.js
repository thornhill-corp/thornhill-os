process.env['ELECTRON_DISABLE_SECURITY_WARNINGS'] = 'true';
const { app, BrowserWindow } = require('electron');
app.once('ready', () => {
    const mainWindow = new BrowserWindow({
        titleBarStyle: 'hidden',
        frame: false,
        kiosk: true,
        icon: './thornhill.ico',
        webPreferences: {
            nodeIntegration: true,
            webSecurity: false
        }
    });
    if(app.isPackaged) return mainWindow.loadFile('./dist/index.html');
    mainWindow.openDevTools();
    new (require('parcel-bundler'))('index.html', { target: 'electron' }).serve(5492)
        .then(() => mainWindow.loadURL('http://localhost:5492/'));
});