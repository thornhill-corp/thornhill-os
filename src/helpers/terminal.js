import { version, dependencies } from '../../package';
import * as lib from '../../lib';

export default async command => {
    command = command.match(/("[^"]+"|[^\s]+)/g);
    const
        name  = command[0],
        args = [],
        opts = {};
    const parse = value => {
        value = value.replace(/"/g, '');
        const valueAsNumber = /^[0-9]+$/.test(value) ? +value : undefined;
        const isValueSafeNumber =
            valueAsNumber
            && valueAsNumber > Number.MIN_SAFE_INTEGER
            && valueAsNumber < Number.MAX_SAFE_INTEGER;
        return isValueSafeNumber ? valueAsNumber : value;
    };
    for(let i = 1; i < command.length; i++){
        const
            current = command[i],
            next = command[i+1];
        if(current.startsWith('--')){
            const
                parsed = current
                    .slice(2)
                    .replace(/-[a-z]/gi, _ => _.toUpperCase())
                    .replace(/-/g, '');
            if(!next || next.startsWith('-'))
                opts[parsed] = true;
            else {
                opts[parsed] = parse(next);
                i++;
            }
        }
        else if(current.startsWith('-')){
            if(current.length === 2){
                const parsed = current[1];
                if(!next || next.startsWith('-'))
                    opts[parsed] = true;
                else {
                    opts[parsed] = parse(next);
                    i++;
                }
            }
            else current.slice(1).split('').forEach(opt => opts[opt] = true);
        }
        else
            args.push(parse(current));
    }
    switch(name){
        case 'about': return `Thornhill O.S. v${version}`;
        case 'shutdown': return require('electron').remote.app.quit();
        case 'run': {
            try {
                lib.run(args[0]);
            }
            catch(error){
                return error.message;
            }
            break;
        }
        case 'help': {
            return ['about', 'shutdown', 'run'].concat(Object.keys(dependencies)
                .filter(d => d.startsWith('@thornhill-corp/'))
                .map(d => d.replace('@thornhill-corp/', ''))
            ).join('\n')
        }
        default: {
            try {
                const res = await require('@thornhill-corp/' + name).default(lib, args, opts);
                if(res) return res.toString();
            }
            catch(error){
                console.error(error);
                const { message } = error;
                return message
                    ? message.startsWith('Cannot find module')
                        ? 'Command not found'
                        : message.split('\n')[0]
                    : undefined;
            }
        }
    }
}