const colors = {
    transparent: 'transparent',
    black:       'black',
    white:       '#DEDEDE',
    gray:        '#959595',
    darkgray:    '#363A3F',
    darkergray:  '#1E2325',
    red:         '#E20000',
    yellow:      '#F2AF16',
    green:       '#01C17C',
    blue:        '#116BFF'
};

const validate = color => Object.keys(colors).includes(color);

const prop = _default => ({
    type: String,
    default: _default,
    validator: validate
});

export default {
    colors,
    validate,
    prop
}