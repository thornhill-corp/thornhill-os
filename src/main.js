import Vue from 'vue';

import App from './App.vue';
import store from './store';

import * as lib from '../lib';

Vue.config.devtools = false;
Vue.config.productionTip = false;

new Vue({
    el: '#app',
    render: h => h(App),
    store
});

require('../lib').run();

Object.keys(lib).forEach(key =>
    Object.defineProperty(window, key, {
        get: () => lib[key],
        enumerable: true
    }));