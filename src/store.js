import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        spaceSize: {
            width: 0,
            height: 0
        },
        output: [],
        isOutputOverlay: false,
        windows: [],
        history: []
    },
    mutations: {
        setSpaceSize: (state, { width, height }) => state.spaceSize = { width, height },
        clearOutput: state => state.output = [],
        replaceOutput: (state, [text, isOverlay]) => {
            state.output = text.split('\n');
            state.isOutputOverlay = isOverlay;
        },
        appendOutput: (state, payload) => state.output.push(...payload.split('\n')),
        addWindow: (state, payload) => state.windows.push(payload),
        removeWindow: (state, payload) => state.windows.splice(state.windows.indexOf(payload), 1),
        clearWindows: state => state.windows = []
    }
});