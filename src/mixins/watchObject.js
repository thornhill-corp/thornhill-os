/*
Properties managed by Object.defineProperty / Object.defineProperties with getters
won't be properly watched by Vue.
I'm looking forward to an alternative for this ugly code,
which purpose is to force watch and refresh.
*/
export default {
    methods: {
        watchObject: function(objectName, callback){
            let oldObject;
            const refresh = () => {
                const newObject = JSON.stringify(this[objectName]);
                if(oldObject !== newObject)
                    callback(oldObject = newObject);
                requestAnimationFrame(refresh);
            };
            refresh();
        }
    }
}