# Thornhill OS

Web-based implementation of the Machine from Person of Interest TV series.

## Features

### Front-end

- [ ] Space
    - [ ] Window
        - [x] Text-only layout
        - [x] Header
            - [x] Title
                - [x] Text * ***
                - [x] Warning
            - [x] Subtitle
                - [x] Text * ** ***
                - [x] Meta text
                - [x] Progress bar
        - [x] Footer
        - [ ] Body
            - [x] Separator
            - [x] Subtitle ***
            - [ ] Text * ** ***
                - [x] Text
                - [ ] Warning
                - [ ] Blinking
                - [ ] Meta text
            - [x] Picture
            - [x] Meter **
            - [x] Table *
                - [x] Title
                - [x] Columns *
            - [x] Countdown
            - [x] Scrolling data
            - [ ] Rotating listd
            - [ ] Audio
        - [ ] Dual column layout
        - [ ] Text glow
        - [ ] Animations
        - [ ] Focus
    - [ ] Feed
        - [ ] Image + sound
            - [ ] MP4
            - [ ] MKV
        - [ ] Image
            - [ ] PNG
            - [ ] JPG
            - [ ] MJPG
        - [ ] Image manipulation
            - [ ] Zoom
            - [ ] Select
            - [ ] Highlight
        - [ ] Sound
            - [ ] MP3
        - [ ] Multi
    - [ ] Window ↔ Feed link
    - [ ] Timeline
    - [ ] World Map
- [x] Output
    - [x] Full screen
    - [x] Overlay
- [ ] Terminal
    - [x] Prompt
    - [x] Output
    - [x] Bash-like behavior
    - [ ] Progress bar
- [ ] Editor
- [ ] Audio effects

\* : Customizable color

** : Customizable background color

*** : Animatable loading dots

### Back-end

- [ ] Record
- [x] Modules
- [ ] Natural programming language
- [ ] Multi-monitor support

## Getting started

### Requirements

- NodeJS v12+
- Yarn

Specific packages to build from Linux :
- `wine-stable`
- `wine32`
- `wine64`

### Install

```
yarn install
yarn start
```

### Build

```
yarn build
```

## Built with

- [Vue](https://github.com/vuejs/vue) : a front-end web framework
- [Parcel](https://github.com/parcel-bundler/parcel/tree/1.x) : a bundler supporting Vue
- [Electron](https://github.com/electron/electron) : a framework for building desktop apps

## Credits

- KaKi87 (Tiana Lemesle) : initial work
- Elarson : graphics inspiration

## Support

Projects of Interest

- [Website](https://thornhill-utilities.com/)
- [Discord](https://thornhill-utilities.com/discord)

## License

Thornhill OS is distributed under the [Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International](LICENSE) (CC BY-NC-SA 4.0) license.

## Changelog

* `1.0.0` (2020-09-06) • Initial release
* `1.1.0` (2020-11-09) • Added random window positioning